<!DOCTYPE html>
<html>
<body>

<input type="text" id="demo" onselect="myFunction(event)" value="Try to copy this text">

<p ></p>

<script>
function myFunction(e) {
e.preventDefault();
var box = document.getElementById("demo");
box.setSelectionRange(box.selectionStart,box.selectionStart);
    
}
</script>

</body>
</html>


<html lang="en"><head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="jquery-clockpicker.min.css">
  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="jquery-clockpicker.min.js"></script>
  <style>
    .modal-dialog {
        width: 100%;
        height: calc(100%-10px);
        padding: 0;
    }
	.clr{
	    background-color: #614d4d;
    border-top: 6px solid #ff0000;
    border-left: 6px solid #00ff08;
    border-right: 6px solid #00ffe7;
    border-bottom: 6px solid yellow;
	}
    #map {
        height: 400px;
		width: 100%;
	}
    .modal-content {
        height: 99%;
    }
    .inner-addon { 
        position: relative; 
    }

    /* style icon */
    .inner-addon .glyphicon {
        position: absolute;
        padding: 10px;
        pointer-events: none;
    }

    /* align icon */
    .left-addon .glyphicon  {
        left:  0px;
	}
    .right-addon .glyphicon {
        right: 0px;
	}

	/* add padding  */
	.left-addon input  { padding-left:  30px; }

	.right-addon input { padding-right: 30px; }

	.weekDays-selector input {
	  display: none!important;
	}

	.weekDays-selector input[type=checkbox] + label {
		display: inline-block;
		border-radius: 9px;
		border: 1px solid #e7e7e7;
		height: 30px;
		width: 30px;
		margin-right: 4px;
	    line-height: 29px;
        text-align: center;
	    cursor: pointer;
	}
	
	
elemt.style {
    background-color: #000e00;
    margin-top: 15px;
    margin-left: 11px;
    display: inline-block;
    width: 20px;
    height: 20px;
    border-top: 6px solid #ff00eb;
    border-left: 6px solid lime;
    border-right: 6px solid #00d0ff;
    border-bottom: 6px solid #fbff00;
    transform: rotate(45deg);
	}

	.weekDays-selector input[type=checkbox]:checked + label {
	    background: #5bc0de;
		    box-shadow: 0 2px 10px rgba(0,0,0,.5);
	    color: #ffffff;
	}
	.lat span{
	margin-left: 4px;
	font: small-caption;
	}
    .lat-long{
		border: 1px solid #ccc;
		border-radius: 5px;
		display: grid;
		width: 80%;
		float: right;
		margin-top: 3px;
		padding: 4px;
	}

    .lat-long label{
	    margin-bottom:0;
	}
	.navv {
    display: inline-flex;
    float: right;
    list-style: none;
    margin-top: 15px;
}
.navv li {
border-right: 1px solid #005fa2;
    margin-right: 34px;
}
.navv li a {
    font-size: 15px;
    color: white;
	font-weight: 600;
    color: #c7cff7;
    margin-right: 10px;
}
  </style>
</head>
<body class="" style="">
    <nav class="navbar navbar-default" style="background-color: #0b5275; border-radius:none;">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
	  <div style="margin-top: 15px;
    margin-left: 11px;
    display: inline-block;
    width: 20px;
    height: 20px;
    border-top: 6px solid red;
    border-left: 6px solid green;
    border-right: 6px solid blue;
    border-bottom: 6px solid yellow;
    transform: rotate(45deg);">
	</div>
      <a class="navbar-brand" style="    font-size: 32px;    border-right: 1px solid #08428a;
    color: white;     letter-spacing: -4px; margin-left: 20px;
    font-weight: bolder;" href="#">OffSellersOn.com</a>
    </div>
    <ul class="navv">
      <li><a href="#"><span class="	glyphicon glyphicon-home"></span> Home</a></li>
      <li><a href="#"><span class="	glyphicon glyphicon-book"></span> About us</a></li>
      <li><a href="#">Services</a></li>
      <li><a href="#">Customers</a></li>
	  <li><a href="#" style="color: white;">Log in <span class="	glyphicon glyphicon-log-in"></span></a></li>
    </ul>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<div class="col-md-offset-5 col-md-6" style="
    border: 1px solid #ececec;
    border-radius: 8px;
">
<div class="col-md-12">
<div class="col-md-6">
	<div class="inner-addon left-addon form-group">
	<span class="	glyphicon glyphicon-home"></span>
    <input type="text" class="form-control" placeholder="Shop Name">
    </div>
    </div>
	<div class="col-md-6">
	<div class="inner-addon left-addon form-group">
	<span class="glyphicon glyphicon-user"></span>
    <input type="text" class="form-control" placeholder="Owner Name">
    </div>
    </div>
</div>
<div class="col-md-12">
	<div class="col-md-6">
		<div class="col-md-6">
		<label>Open Time:</label>
	<div class="input-group clockpicker">
    <input type="text" class="form-control" value="09:30">
    <span class="input-group-addon">
        <span class="glyphicon glyphicon-time"></span>
    </span>
</div>
</div>
<div class="col-md-6">
		<label>Close Time:</label>
	<div class="input-group clockpicker">
    <input type="text" class="form-control" value="09:30">
    <span class="input-group-addon">
        <span class="glyphicon glyphicon-time"></span>
    </span>
	</div>
</div>
	</div>
			<div class="col-md-6">
			<label>Shop working days:</label>
<div class="weekDays-selector" style="
    margin-top: 1px;
">
  <input type="checkbox" id="weekday-mon" class="weekday">
  <label for="weekday-mon">M</label>
  <input type="checkbox" id="weekday-tue" class="weekday">
  <label for="weekday-tue">T</label>
  <input type="checkbox" id="weekday-wed" class="weekday">
  <label for="weekday-wed">W</label>
  <input type="checkbox" id="weekday-thu" class="weekday">
  <label for="weekday-thu">T</label>
  <input type="checkbox" id="weekday-fri" class="weekday">
  <label for="weekday-fri">F</label>
  <input type="checkbox" id="weekday-sat" class="weekday">
  <label for="weekday-sat">S</label>
  <input type="checkbox" id="weekday-sun" class="weekday">
  <label for="weekday-sun">S</label>
</div>
    </div>
	</div>
	<div class="modal fade" id="myModal" role="dialog" style="display: none;">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">×</button>
			<div class="col-md-3">
				<div class="lat-long">
 <label class="lat">Lattitude :<span id="lat"></span></label>
 <label class="lat">Longitude:<span id="lng"></span></label>
</div>
</div>
<div class="col-md-1">
				<a class="btn btn-info" onclick="getGoogleMapAddress()" style="margin: 12px;" href="#">
					<i class="glyphicon glyphicon-arrow-right"></i>
				</a>
				</div>
				<div class="col-md-4">
				<textarea class="form-control" rows='2' id="address"></textarea>
				</div>
				<div class="col-md-1 col-md-offset-1"><a href="#" style="margin:8px;" class="btn btn-info">
          <span class="glyphicon glyphicon-screenshot"></span> Mapped
        </a></div>
        </div>
        <div class="modal-body">
		<div id="map"></div>
      </div>
    </div>
  </div>
  </div>
<div class="col-md-12">
	<div class="col-md-6" style="
    margin-top: 20px;
">

	<div class="form-group">
		<span style="display:inline;">
		<label>Shop Address:</label>
		        <a href="#" class="btn btn-info" data-toggle="modal" data-target="#myModal" aria-hidden="true">
          <span class="glyphicon glyphicon-map-marker"></span> Map
        </a>
		</span>
    </div>
</div>
</div>
	<div class="col-md-12">
	<div class="col-md-6" style="
    margin-top: 20px;
">

	<div class="form-group">
		<span style="display:inline;">
		<label>Password:</label><label id='success' style="float: right;color: green;"></label>
		        <input id='pwd' type='password' onfocusout='showConfirmPassword(function (){$("#confirm").focus();console.log("nana");});' class="form-control"/>
		</span>
    </div>
</div>
	<div class="col-md-6" id='confirm' style="
    margin-top: 20px; display: none;
">	

	<div class="form-group">
		<label>Confirm Password:</label>
		        <input id='cfmPWD' onkeyup='confirmPassword();' type='password' class="form-control"/>
    </div>
</div>
</div>
	<div class="col-md-12">
	<div class="col-md-12">
	<div class="inner-addon left-addon form-group">
			<label>Words About Shop:</label>
    <textarea class="form-control" id="den"></textarea>
    </div>
	</div>
    </div>
	<div class="col-md-12 center-block">
	<button class="btn btn-primary center-block" style="font-size: 20px;">Create my Shop</button>
	</div>
	</div>

<script type="text/javascript">
function confirmPassword() {
	if(document.getElementById('pwd').value === document.getElementById('cfmPWD').value){
		$("#confirm").hide();
		document.getElementById('success').innerText = "OK"
	}
}

function showConfirmPassword() {
	if (document.getElementById('pwd').value !== '') {
		$("#confirm").show();
		$("#cfmPWD").focus();
	}
}

$('.clockpicker').clockpicker();

function getGoogleMapAddress() {
var lat = document.getElementById("lat").innerText;
var lng = document.getElementById("lng").innerText;
        var latlng = {lat: parseFloat(lat), lng: parseFloat(lng)};
		var geocoder = new google.maps.Geocoder;
        geocoder.geocode({'location': latlng}, function(results, status) {
          if (status === 'OK') {
            if (results[0]) {
                document.getElementById("address").value = results[0].formatted_address;
            } else {
              window.alert('No results found');
            }
          } else {
            window.alert('Geocoder failed due to: ' + status);
          }
        });
}

function myMap() {
var mapOptions = {
    center: new google.maps.LatLng(51.5, -0.12),
    zoom: 10,
    mapTypeId: google.maps.MapTypeId.HYBRID
}
var map = new google.maps.Map(document.getElementById("map"), mapOptions);
}
</script>
        <script>var map; //Will contain map object.
var marker = false; ////Has the user plotted their location marker? 
        
//Function called to initialize / create the map.
//This is called when the page has loaded.
function initMap() {
 
    //The center location of our map.
    var centerOfMap = new google.maps.LatLng(52.357971, -6.516758);
 
    //Map options.
    var options = {
      center: centerOfMap, //Set center.
      zoom: 7 //The zoom value.
    };
 
    //Create the map object.
    map = new google.maps.Map(document.getElementById('map'), options);
 
    //Listen for any clicks on the map.
    google.maps.event.addListener(map, 'click', function(event) {                
        //Get the location that the user clicked.
        var clickedLocation = event.latLng;
        //If the marker hasn't been added.
        if(marker === false){
            //Create the marker.
            marker = new google.maps.Marker({
                position: clickedLocation,
                map: map,
                draggable: true //make it draggable
            });
            //Listen for drag events!
            google.maps.event.addListener(marker, 'dragend', function(event){
                markerLocation();
            });
        } else{
            //Marker has already been added, so just change its location.
            marker.setPosition(clickedLocation);
        }
        //Get the marker's location.
        markerLocation();
    });
}
        
//This function will get the marker's current location and then add the lat/long
//values to our textfields so that we can save the location.
function markerLocation(){
    //Get location.
    var currentLocation = marker.getPosition();
    //Add lat and lng values to a field that we can save.
    document.getElementById('lat').innerText = currentLocation.lat(); //latitude
    document.getElementById('lng').innerText = currentLocation.lng(); //longitude
}
        
        
//Load the map when the page has finished loading.
google.maps.event.addDomListener(window, 'load', initMap);</script>
</body></html>